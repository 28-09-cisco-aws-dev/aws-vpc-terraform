resource "aws_vpc" "lab_vpc" {
  cidr_block = "10.0.0.0/24"

  tags = {
    Name = "lab_vpc"
  }
}