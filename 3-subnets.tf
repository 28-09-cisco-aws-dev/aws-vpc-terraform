resource "aws_subnet" "public-ap-south-1a" {
  vpc_id            = aws_vpc.lab_vpc.id
  cidr_block        = "10.0.0.0/26"
  availability_zone = "ap-south-1a"
  map_public_ip_on_launch = "true"

  tags = {
    "Name"                            = "public-a"
  }
}

resource "aws_subnet" "private-ap-south-1a" {
  vpc_id            = aws_vpc.lab_vpc.id
  cidr_block        = "10.0.0.64/26"
  availability_zone = "ap-south-1a"

  tags = {
    "Name"                            = "private-a"
  }
}

resource "aws_subnet" "public-ap-south-1b" {
  vpc_id            = aws_vpc.lab_vpc.id
  cidr_block        = "10.0.0.128/26"
  availability_zone = "ap-south-1b"

  tags = {
    "Name"                            = "public-b"
  }
}

resource "aws_subnet" "private-ap-south-1b" {
  vpc_id            = aws_vpc.lab_vpc.id
  cidr_block        = "10.0.0.192/26"
  availability_zone = "ap-south-1b"

  tags = {
    "Name"                            = "private-b"
  }
}